<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index')->name('home.admin');
    Route::get('/login', 'AdminController@login')->name('login');
    Route::get('/menu', 'AdminController@buatMenu')->name('buat.menu');
    Route::get('/pesanan/{id}', 'AdminController@show');
    Route::POST('/addPost','AdminController@addPost');
    Route::POST('/editPost','AdminController@editPost')->name('edit.menu');
    Route::POST('/deletePost','AdminController@deletePost')->name('delete.menu');
    Route::get('/pesanan', 'AdminController@pesanan')->name('pesanan.client');
    Route::get('/logout', 'AdminController@logout')->name('logout.admin');
});
