<?php

namespace App\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use Validator;
use Response;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Models\Menu;
use App\Modules\Cashier\Models\Order;

class AdminController extends Controller
{
    public function __construct()	{
		$this->middleware('auth', ['except' => ['login']]);
        $this->middleware('role:admin', ['except' => ['login']]);
	}

    public function index()
    {
        return view('admin::pages.home');
    }

    public function login()
    {
    	return redirect('/auth/login');
    }

    public function buatMenu()
    {
        $post = Menu::all();
        return view('admin::pages.menu', compact('post'));
    }

    public function addPost(Request $request){
        $rules = array(
          'jenis' => 'required',
          'desc' => 'required',
          'harga' => 'required',
          'status' => 'required',
        );

      $validator = Validator::make(Input::all(), $rules);
      if ($validator->fails()){
        return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));
      }else {
        $post = new Menu;
        $post->menus_jenis = $request->jenis;
        $post->menus_desc = $request->desc;
        $post->menus_harga = $request->harga;
        $post->menus_status = $request->status;
        $post->save();
        return response()->json($post);
      }
    }

    public function editPost(request $request){
        $post = Menu::find($request->id);
        $post->menus_jenis = $request->jenis;
        $post->menus_desc = $request->desc;
        $post->menus_harga = $request->harga;
        $post->menus_status = $request->status;
        $post->save();
        return response()->json($post);
    }

    public function deletePost(request $request){
        $post = Menu::find($request->id)->delete();
        return response()->json();
    }

    public function pesanan()
    {
        $menu = Menu::where('menus_status', 'ready')->get();
        $orders = DB::table('menus') 
            ->join('orders', 'menus.menus_id', '=', 'orders.orders_menu_id')
            ->select('menus.menus_desc', 'menus.menus_harga', 'orders.*')
            ->groupBy('orders.orders_customer')
            ->get();
        return view('admin::pages.pesanan', compact('orders'));
    }

    public function logout()
    {
    	Auth::logout();
    	return redirect('/auth/login');
    }

    public function show($id)
    {
        $id = Order::find($id);
        $menu = Menu::where('menus_status', 'ready')->get();
        $orders = DB::table('menus') 
            ->join('orders', 'menus.menus_id', '=', 'orders.orders_menu_id')
            ->select('menus.menus_desc', 'menus.menus_harga','menus.menus_id' , 'orders.*')
            ->where('orders.orders_no_pesanan', '=', $id->orders_no_pesanan)
            ->get();

        return view('admin::pages.detail', compact('menu', 'orders','id'));
    }
}
