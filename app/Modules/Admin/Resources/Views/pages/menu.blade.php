@extends('admin::layouts.app')
@section('content')
	<section id="main-content">
		<section class="wrapper">
			{{-- @if(session('msg'))
				<div class="alert alert-success">
					<p>{{ session('msg') }}</p>
				</div>
			@endif --}}

			{{-- <a class="btn btn-success" data-toggle="modal" href="#myModal" style="margin: 20px 0;">
	            Tambah Menu
	        </a> --}}

	        <button type="button" class="create-modal btn btn-success btn-sm">
            	<i class="glyphicon glyphicon-plus"></i>
          	</button>
			
			<div class="row">
	          <div class="col-lg-12">
	            <section class="panel">
	              <header class="panel-heading">
	                Daftar Menu Restoran
	              </header>

	              <table id="menuTable" class="display">
	              	<thead>
	                  <tr>
	                    <th> Jenis</th>
	                    <th> Deskripsi</th>
	                    <th> Harga (@Porsi)</th>
	                    <th> Status</th>
	                    <th><i class="icon_cogs"></i> Action</th>
	                  </tr>
	                </thead>
	                <tbody>
					  {{ csrf_field() }}
	                  @foreach ($post as $value)
	                  <tr class="post{{$value->menus_id}}">
	                    <td>{{ $value->menus_jenis }}</td>
	                    <td>{{ $value->menus_desc }}</td>
	                    <td>{{ $value->menus_harga }}</td>
	                    <td>{{ $value->menus_status }}</td>
	                    <td>
	                      <div class="btn-group">
	                      	<button type="button" class="show-modal btn btn-info btn-sm" 
	                      				data-id="{{$value->menus_id}}" 
	                      				data-jenis="{{$value->menus_jenis}}" 
	                      				data-desc="{{$value->menus_desc}}"
	                      				data-harga="{{$value->menus_harga}}"
	                      				data-status="{{$value->menus_status}}">
				              <i class="fa fa-eye"></i>
				            </button>
				            <button type="button" class="edit-modal btn btn-warning btn-sm" 
				            			data-id="{{$value->menus_id}}" 
	                      				data-jenis="{{$value->menus_jenis}}" 
	                      				data-desc="{{$value->menus_desc}}"
	                      				data-harga="{{$value->menus_harga}}"
	                      				data-status="{{$value->menus_status}}">
				              <i class="glyphicon glyphicon-pencil"></i>
				            </button>
				            <button type="button" class="delete-modal btn btn-danger btn-sm" 
				            			data-id="{{$value->menus_id}}" 
	                      				data-jenis="{{$value->menus_jenis}}" 
	                      				data-desc="{{$value->menus_desc}}"
	                      				data-harga="{{$value->menus_harga}}"
	                      				data-status="{{$value->menus_status}}">
				              <i class="glyphicon glyphicon-trash"></i>
				            </button>
	                      </div>
	                    </td>
	                  </tr>
	                  @endforeach
	                </tbody>

	              </table>
	            </section>
	            {{-- {{ $post->links() }} --}}
	          </div>
	          
	        </div>

            {{-- Modal Form Create Post --}}
			<div id="create" class="modal fade" role="dialog">
			  <div class="modal-dialog" style="left: 40px;">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title"></h4>
			      </div>
			      <div class="modal-body">
			        <form class="form-horizontal" role="form">
			          <div class="form-group row add">
			            <label class="control-label col-sm-2" for="jenis">Jenis :</label>
			            <div class="col-sm-10">
			              <select class="form-control" id="jenis" name="jenis" required="">
                              <option value="makanan">Makanan</option>
                              <option value="minuman">Minuman</option>
                          </select>
			              <p class="error text-center alert alert-danger hidden"></p>
			            </div>
			          </div>
			          <div class="form-group">
			            <label class="control-label col-sm-2" for="desc">Desc :</label>
			            <div class="col-sm-10">
			              <textarea class="form-control" id="desc" name="desc" required></textarea>
			              <p class="error text-center alert alert-danger hidden"></p>
			            </div>
			          </div>
			          <div class="form-group">
			            <label class="control-label col-sm-2" for="harga">Harga :</label>
			            <div class="col-sm-10">
			              <input class="form-control" type="number" id="harga" name="harga" required>
			              <p class="error text-center alert alert-danger hidden"></p>
			            </div>
			          </div>
			          <div class="form-group row add">
			            <label class="control-label col-sm-2" for="status">Status :</label>
			            <div class="col-sm-10">
			              <select class="form-control" id="status" name="status" required="">
                              <option value="proses">Proses</option>
                              <option value="ready">Ready</option>
                          </select>
			              <p class="error text-center alert alert-danger hidden"></p>
			            </div>
			          </div>
			        </form>
			      </div>
			          <div class="modal-footer">
			            <button class="btn btn-warning" type="submit" id="add">
			              <span class="glyphicon glyphicon-plus"></span>Save Post
			            </button>
			            <button class="btn btn-warning" type="button" data-dismiss="modal">
			              <span class="glyphicon glyphicon-remobe"></span>Close
			            </button>
			          </div>
			      </div>
			    </div>
			  </div>
			</div>
			
            {{-- Modal Form Show POST --}}
			<div id="show" class="modal fade" role="dialog">
			  <div class="modal-dialog" style="left: 40px;">
			    <div class="modal-content">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal">&times;</button>
			          	<h4 class="modal-title"></h4>
			        </div>
			                    <div class="modal-body">
			                    <div class="form-group">
			                      <label for="">ID :</label>
			                      <b id="id-post"/>
			                    </div>
			                    <div class="form-group">
			                      <label for="">Jenis :</label>
			                      <b id="jenis-post"/>
			                    </div>
			                    <div class="form-group">
			                      <label for="">Deskripsi :</label>
			                      <b id="desc-post"/>
			                    </div>
			                    <div class="form-group">
			                      <label for="">Harga :</label>
			                      <b id="harga-post"/>
			                    </div>
			                    <div class="form-group">
			                      <label for="">Status :</label>
			                      <b id="status-post"/>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>

			{{-- Modal Form Edit and Delete Post --}}
			<div id="myModal" class="modal fade" role="dialog">
			  <div class="modal-dialog" style="left: 40px;">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title"></h4>
			      </div>
			      <div class="modal-body">
			        <form class="form-horizontal" role="modal">

			          <div class="form-group">
			            <label class="control-label col-sm-2" for="id">ID</label>
			            <div class="col-sm-10">
			              <input type="text" class="form-control" id="menus_id" disabled>
			            </div>
			          </div>

			          <div class="form-group">
			            <label class="control-label col-sm-2" for="jenis">Jenis</label>
			            <div class="col-sm-10">
                          <select class="form-control" id="menus_jenis">
                              <option value="makanan">Makanan</option>
                              <option value="minuman">Minuman</option>
                          </select>
			            </div>
			          </div>

			          <div class="form-group">
			            <label class="control-label col-sm-2" for="desc">Desc</label>
			            <div class="col-sm-10">
			            	<textarea type="text" class="form-control" id="menus_desc"></textarea>
			            </div>
			          </div>
			          <div class="form-group">
			            <label class="control-label col-sm-2" for="harga">Harga</label>
			            <div class="col-sm-10">
			              <input type="text" class="form-control" id="menus_harga">
			            </div>
			          </div>
			          <div class="form-group">
			            <label class="control-label col-sm-2" for="status">Status</label>
			            <div class="col-sm-10">
			              <select class="form-control" id="menus_status">
                              <option value="proses">Proses</option>
                              <option value="ready">Ready</option>
                          </select>
			            </div>
			          </div>

			        </form>
			        
			        {{-- Form Delete Post --}}
			        <div class="deleteContent">
			          Yakin ingin menghapus <span class="title"></span>?
			          <span class="hidden id"></span>
			        </div>

			      </div>
			      <div class="modal-footer">

			        <button type="button" class="btn actionBtn" data-dismiss="modal">
			          <span id="footer_action_button" class="glyphicon"></span>
			        </button>

			        <button type="button" class="btn btn-warning" data-dismiss="modal">
			          <span class="glyphicon glyphicon"></span>close
			        </button>

			      </div>
			    </div>
			  </div>
			</div>


		</section>
	</section>
@endsection
