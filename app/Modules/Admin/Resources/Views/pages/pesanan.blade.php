@extends('admin::layouts.app')
@section('content')
	<section id="main-content">
		<section class="wrapper">
			@if(session('msg'))
				<div class="alert alert-success">
					<p>{{ session('msg') }}</p>
				</div>
			@endif
			
			<div class="row">
	          <div class="col-lg-12">
	            <section class="panel">
	              <header class="panel-heading">
	                Daftar Pesanan Restoran
	              </header>

	              <table class="table table-striped table-advance table-hover display" id="tableAdmin">
	                <thead>
	                  <tr>
	                    <th> Customer</th>
	                    <th> No. Pesanan</th>
	                    <th> No. Meja</th>
	                    <th><i class="icon_cogs"></i> Action</th>
	                  </tr>
	                </thead>
					<tbody>
	                  @foreach($orders as $val)
	                  <tr>
	                    <td>{{ $val->orders_customer }}</td>
	                    <td>{{ $val->orders_no_pesanan }}</td>
	                    <td>{{ $val->orders_no_meja }}</td>
	                    <td>
	                        <a class="btn btn-success" href="{{ url('admin/pesanan') }}/{{ $val->orders_id }}"><i class="fa fa-eye"></i> Detail</a>
	                    </td>
	                  </tr>
	                  @endforeach
	                </tbody>
	              </table>
	            </section>
	          </div>
	        </div>


		</section>
	</section>
@endsection