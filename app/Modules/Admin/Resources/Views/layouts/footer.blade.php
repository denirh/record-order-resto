
</section>
<!-- javascripts -->
  <script src="{{asset('master/js/jquery.js')}}"></script>
  <script src="{{asset('master/js/jquery-ui-1.10.4.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('master/js/jquery-ui-1.9.2.custom.min.js')}}"></script>
  <!-- bootstrap -->
  <script src="{{asset('master/js/bootstrap.min.js')}}"></script>
  <!-- nice scroll -->
  <script src="{{asset('master/js/jquery.scrollTo.min.js')}}"></script>
  <script src="{{asset('master/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
  <!-- charts scripts -->
  <script src="{{asset('master/assets/jquery-knob/js/jquery.knob.js')}}"></script>
  <script src="{{asset('master/js/jquery.sparkline.js')}}" type="text/javascript"></script>
  <script src="{{asset('master/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js')}}"></script>
  <script src="{{asset('master/js/owl.carousel.js')}}"></script>
  <!-- jQuery full calendar -->
  <script src="{{asset('master/js/fullcalendar.min.js')}}"></script>
    <!-- Full Google Calendar - Calendar -->
    <script src="{{asset('master/assets/fullcalendar/fullcalendar/fullcalendar.js')}}"></script>
    <!--script for this page only-->
    <script src="{{asset('master/js/calendar-custom.js')}}"></script>
    <script src="{{asset('master/js/jquery.rateit.min.js')}}"></script>
    <!-- custom select -->
    <script src="{{asset('master/js/jquery.customSelect.min.js')}}"></script>
    <script src="{{asset('master/assets/chart-master/Chart.js')}}"></script>
    <!--custome script for all page-->
    <script src="{{asset('master/js/scripts.js')}}"></script>
    <!-- custom script for this page-->
    <script src="{{asset('master/js/sparkline-chart.js')}}"></script>
    <script src="{{asset('master/js/easy-pie-chart.js')}}"></script>
    <script src="{{asset('master/js/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{asset('master/js/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('master/js/xcharts.min.js')}}"></script>
    <script src="{{asset('master/js/jquery.autosize.min.js')}}"></script>
    <script src="{{asset('master/js/jquery.placeholder.min.js')}}"></script>
    <script src="{{asset('master/js/gdp-data.js')}}"></script>
    <script src="{{asset('master/js/morris.min.js')}}"></script>
    <script src="{{asset('master/js/sparklines.js')}}"></script>
    <script src="{{asset('master/js/charts.js')}}"></script>
    <script src="{{asset('master/js/jquery.slimscroll.min.js')}}"></script>
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script --}}>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script> 
    <script>

      $(document).ready( function () {
          $('#tableAdmin').DataTable();
      } );
      
      //knob
      $(function() {
        $(".knob").knob({
          'draw': function() {
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
        $("#owl-slider").owlCarousel({
          navigation: true,
          slideSpeed: 300,
          paginationSpeed: 400,
          singleItem: true

        });
      });

      //custom select box

      $(function() {
        $('select.styled').customSelect();
      });

      /* ---------- Map ---------- */
      $(function() {
        $('#map').vectorMap({
          map: 'world_mill_en',
          series: {
            regions: [{
              values: gdpData,
              scale: ['#000', '#000'],
              normalizeFunction: 'polynomial'
            }]
          },
          backgroundColor: '#eef3f7',
          onLabelShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
          }
        });
      });

    
      // --------------- Read data ----------------
        //Show function
          $(document).on('click', '.show-modal', function() {
          $('#show').modal('show');
          $('#id-post').text($(this).data('id'));
          $('#jenis-post').text($(this).data('jenis'));
          $('#desc-post').text($(this).data('desc'));
          $('#harga-post').text($(this).data('harga'));
          $('#status-post').text($(this).data('status'));
          $('.modal-title').text('Informasi Menu');
        });

      // --------------- Edit Data -----------------
        //function Edit POST
        $(document).on('click', '.edit-modal', function() {
        $('#footer_action_button').text(" Update");
        $('#footer_action_button').addClass('glyphicon-check');
        $('#footer_action_button').removeClass('glyphicon-trash');
        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.actionBtn').addClass('edit');
        $('.modal-title').text('Edit Menu');
        $('.deleteContent').hide();
        $('.form-horizontal').show();
        $('#menus_id').val($(this).data('id'));
        $('#menus_jenis').val($(this).data('jenis'));
        $('#menus_desc').val($(this).data('desc'));
        $('#menus_harga').val($(this).data('harga'));
        $('#menus_status').val($(this).data('status'));
        $('#myModal').modal('show');
        });

        $('.modal-footer').on('click', '.edit', function() {
          $.ajax({
            type: 'POST',
            url: 'editPost',
            data: {
        '_token': $('input[name=_token]').val(),
          id: $("#menus_id").val(),
          jenis: $('#menus_jenis').val(),
          desc: $('#menus_desc').val(),
          harga: $('#menus_harga').val(),
          status: $('#menus_status').val()
        },
        success: function(data) {
              $('.post' + data.id).replaceWith(" "+
              "<tr class='post" + data.id + "'>"+
              "<td>" + data.menus_jenis + "</td>"+
              "<td>" + data.menus_desc + "</td>"+
              "<td>" + data.menus_harga + "</td>"+
              "<td>" + data.menus_status + "</td>"+
         "<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-jenis='" + data.menus_jenis + "' data-desc='" + data.menus_desc + "' data-harga='" + data.menus_harga + "' data-status='" + data.menus_status + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-jenis='" + data.menus_jenis + "' data-desc='" + data.menus_desc + "' data-harga='" + data.menus_harga + "' data-status='" + data.menus_status + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-jenis='" + data.menus_jenis + "' data-desc='" + data.menus_desc + "' data-harga='" + data.menus_harga + "' data-status='" + data.menus_status + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+ "</tr>");
           $('#myModal').modal('hide');
            }
          });
        });

        // form Delete function
          $(document).on('click', '.delete-modal', function() {
          $('#footer_action_button').text(" Delete");
          $('#footer_action_button').removeClass('glyphicon-check');
          $('#footer_action_button').addClass('glyphicon-trash');
          $('.actionBtn').removeClass('btn-success');
          $('.actionBtn').addClass('btn-danger');
          $('.actionBtn').addClass('delete');
          $('.modal-title').text('Delete Post');
          $('.id').text($(this).data('id'));
          $('.deleteContent').show();
          $('.form-horizontal').hide();
          $('.title').html($(this).data('title'));
          $('#myModal').modal('show');
          });

          $('.modal-footer').on('click', '.delete', function(){
            $.ajax({
              type: 'POST',
              url: 'deletePost',
              data: {
                '_token': $('input[name=_token]').val(),
                'id': $('.id').text()
              },
              success: function(data){
                 $('.post' + $('.id').text()).remove();
              }
            });
          });

        {{-- ajax Form Add Post--}}
          $(document).on('click','.create-modal', function() {
            $('#create').modal('show');
            $('.form-horizontal').show();
            $('.modal-title').text('Add Post');
          });
          $("#add").click(function() {
            console.log('ok');
            $.ajax({
              type: 'POST',
              url: 'addPost',
              data: {
                '_token': $('input[name=_token]').val(),
                jenis: $('select[name=jenis]').val(),
                desc: $('textarea[name=desc]').val(),
                harga: $('input[name=harga]').val(),
                status: $('select[name=status]').val()
              },
              success: function(data){
                if ((data.errors)) {
                  $('.error').removeClass('hidden');
                  $('.error').text(data.errors.jenis);
                  $('.error').text(data.errors.desc);
                  $('.error').text(data.errors.harga);
                  $('.error').text(data.errors.status);
                } else {
                  $('.error').remove();
                  $('#table').append("<tr class='post" + data.id + "'>"+
                  
                  "<td>" + data.menus_jenis + "</td>"+
                  "<td>" + data.menus_desc + "</td>"+
                  "<td>" + data.menus_harga + "</td>"+
                  "<td>" + data.menus_status + "</td>"+
                  
                  "<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.menus_id + "' data-jenis='" + data.menus_jenis + "' data-desc='" + data.menus_desc + "' data-harga='" + data.menus_harga + "' data-status='" + data.menus_status + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.menus_id + "' data-jenis='" + data.menus_jenis + "' data-desc='" + data.menus_desc + "' data-harga='" + data.menus_harga + "' data-status='" + data.menus_status + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-jenis='" + data.menus_jenis + "' data-desc='" + data.menus_desc + "' data-harga='" + data.menus_harga + "' data-status='" + data.menus_status + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+ "</tr>");
                   $('#create').modal('hide');
                }
              },
            });
            $('#jenis').val('');
            $('#desc').val('');
            $('#harga').val('');
            $('#status').val('');

          });

    </script>

</body>

</html>