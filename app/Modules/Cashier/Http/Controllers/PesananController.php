<?php

namespace App\Modules\Cashier\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Cashier\Models\Menu;
use App\Modules\Cashier\Models\Order;
use App\Modules\Cashier\Models\Transaction;

class PesananController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:kasir');
    }

    public function index()
    {
        $menu = Menu::where('menus_status', 'ready')->get();
        $orders = DB::table('menus') 
            ->join('orders', 'menus.menus_id', '=', 'orders.orders_menu_id')
            ->select('menus.menus_desc', 'menus.menus_harga', 'orders.*')
            ->groupBy('orders.orders_customer')
            ->get();
        return view('cashier::pages.pesanan', compact('menu', 'orders'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $menu= Menu::all();
        $i=0;
        $orders = '';
        $tgl=date('dmY');
        $kode_pesan = 'ERP'.$tgl.'-';

        foreach($menu as $value){
            $menus=$value->menus_id;
                if($request->$menus!=''){
                    $value->menus_desc = Order::create([        
                        'orders_customer'       => $request->customer,
                        'orders_no_pesanan'     => $kode_pesan.$request->no_pesanan,
                        'orders_no_meja'        => $request->no_meja,            
                        'orders_menu_id'        => $value->menus_id,            
                        'orders_jumlah_pesan'   => $request->$menus,
                    ]);

                    $value->menus_desc->save();
                    $i++;
                }
        }

        return redirect('/cashier/pesanan');   
        
    }

    public function show($id)
    {
        $id = Order::find($id);
        $menu = Menu::where('menus_status', 'ready')->get();
        $orders = DB::table('menus') 
            ->join('orders', 'menus.menus_id', '=', 'orders.orders_menu_id')
            ->select('menus.menus_desc', 'menus.menus_harga','menus.menus_id' , 'orders.*')
            ->where('orders.orders_no_pesanan', '=', $id->orders_no_pesanan)
            ->get();

        return view('cashier::pages.detail', compact('menu', 'orders','id'));
    }

    public function edit($id)
    {
        $id = Order::find($id);
        $menu = Menu::where('menus_status', 'ready')->get();
        $orders = DB::table('menus') 
            ->join('orders', 'menus.menus_id', '=', 'orders.orders_menu_id')
            ->select('menus.menus_desc', 'menus.menus_harga', 'orders.*')
            ->where('orders.orders_no_pesanan', '=', $id->orders_no_pesanan)
            ->get();

        return view('cashier::pages.edit', compact('menu', 'orders', 'id'));
    }


    public function update(Request $request)
    {
        $menu= Menu::all();
        $i=0;
        $order = '';


            foreach($menu as $value){
              $menus=$value->menus_id;
                if($request->$menus!=''){
                    $found=false;
                    $findOrder=Order::where('orders_no_meja','=',$request->no_meja)->where('orders_no_pesanan','=',$request->no_pesanan)->get();
                    foreach ($findOrder as $value1) {
                        if ($value1->orders_menu_id==$menus) {
                            $found=true;
                        }
                       
                    }
                    
                    if ($found==true) {
                    $findId=Order::where('orders_no_meja','=',$request->no_meja)->where('orders_no_pesanan','=',$request->no_pesanan)->where('orders_menu_id','=',$menus)->first();
                    $order.$i = Order::find($findId->orders_id);
                    $order.$i->orders_customer = $request->customer;
                    $order.$i->orders_no_meja = $request->no_meja;
                    $order.$i->orders_menu_id = $value->menus_id;
                    $order.$i->orders_jumlah_pesan = $request->$menus;
                    $order.$i->save();
                    }else{
                        $order.$i=Order::create([        
                        'orders_customer'       => $request->customer,
                        'orders_no_pesanan'     => $request->no_pesanan,
                        'orders_no_meja'        => $request->no_meja,           
                        'orders_menu_id'        => $value->menus_id,            
                        'orders_jumlah_pesan'   => $request->$menus
                    ]);
                    }
                    $i++;
                   
                }
            }

        return redirect('/cashier/pesanan')->with('msg', 'Pesanan berhasil di update !');   
    }


    public function destroy($id)
    {
        $del='';
        $order = DB::table('orders')->where('orders_id','=',$id)->first();
        $orders = DB::table('orders')->where('orders_no_pesanan','=',$order->orders_no_pesanan)->get();
        foreach ($orders as $key) {
            $del.$i=DB::table('orders')->where('orders_id','=',$key->orders_id)->delete();
        }

        return redirect('/cashier/pesanan')->with('msg', 'Pesanan berhasil di hapus !');
    }

    public function transaction(Request $request)
    {        
        // $i=0;
        // $order = DB::table('orders')->where('orders_id','=',$id)->first();
        // $orders = DB::table('orders')->where('orders_no_pesanan','=',$order->orders_no_pesanan)->get();
        // foreach ($orders as $key) {
        
        $bayar='';
            $bayar = Transaction::create([
                    'transactions_no_pesanan' =>$request->no_pesan,
                    'transactions_harga' => $request->bayar,
                    'transactions_status' => 'finish'
            ]);
        $bayar->save();
        

        
         
        // $trans = Transaction::create([
        //     'transactions_harga' => $request->bayar,
        //     'transactions_status' => 'finish'
        // ]);

        // $trans->save();

         return redirect('/cashier/transaksi')->with('msg', 'Berhasil Transaksi !');      
        
    }
}
