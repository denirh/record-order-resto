<?php

namespace App\Modules\Cashier\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Modules\Cashier\Models\Menu;
use App\Modules\Cashier\Models\Order;
use App\Modules\Cashier\Models\Transaction;

class CashierController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('role:kasir');
	}

    public function index()
    {
    	$data = Menu::all();
        return view('cashier::pages.home', compact('data'));
    }

    public function login()
    {
    	return view('auth::auth.login');
    }

    public function logout()
    {
        \Auth::logout();
        return redirect('/auth/login');
    }

    public function transaksi()
    {
         
        $transaksi = Transaction::all();
        $info_transaksi = DB::table('transactions')
                          ->join('orders', 'transactions.transactions_no_pesanan', 'orders.orders_no_pesanan')
                          ->select('transactions.*', 'orders.*')
                          ->groupBy('orders.orders_no_pesanan')
                          ->get();
        //dd($info_transaksi);

        return view('cashier::pages.transaksi', compact('transaksi', 'info_transaksi'));
    }

}
