<?php

namespace App\Modules\Cashier\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Cashier\Models\Menu;

class MenuController extends Controller
{
    public function index()
    {
        $menu = Menu::where('menus_status', 'ready')->get();
        return view('cashier::pages.menu', compact('menu'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(request $request)
    {
        
    }

    public function destroy(request $request)
    {
        
    }

}
