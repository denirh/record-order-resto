<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'cashier'], function () {
    Route::get('/', 'CashierController@index')->name('home.cashier');
    Route::get('/login', 'CashierController@loginForm');

   // Route::get('/menu', 'MenuController@index')->name('menu');

    Route::get('/menu', 'MenuController@index')->name('menu');
    Route::post('/addMenu', 'MenuController@store');
    Route::post('/editMenu', 'MenuController@update');
    Route::post('/hapusMenu', 'MenuController@destroy');

    Route::resource('/pesanan', 'PesananController');
    Route::get('/pesanan/{id}', 'PesananController@show')->name('single.info');
    Route::get('/edit/{id}', 'PesananController@edit');
    Route::post('update/', 'PesananController@update');
    Route::get('/hapus/{id}', 'PesananController@destroy');

    Route::get('/transaksi', 'CashierController@transaksi')->name('transaksi.client');
    Route::post('bayar/{id}', 'PesananController@transaction');
    Route::get('/logout', 'CashierController@logout');
});
