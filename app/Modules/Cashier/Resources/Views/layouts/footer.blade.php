
</section>
<!-- javascripts -->
  <script src="{{asset('master/js/jquery.js')}}"></script>
  <script src="{{asset('master/js/jquery-ui-1.10.4.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('master/js/jquery-ui-1.9.2.custom.min.js')}}"></script>
  <!-- bootstrap -->
  <script src="{{asset('master/js/bootstrap.min.js')}}"></script>
  <!-- nice scroll -->
  <script src="{{asset('master/js/jquery.scrollTo.min.js')}}"></script>
  <script src="{{asset('master/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
  <!-- charts scripts -->
  <script src="{{asset('master/assets/jquery-knob/js/jquery.knob.js')}}"></script>
  <script src="{{asset('master/js/jquery.sparkline.js')}}" type="text/javascript"></script>
  <script src="{{asset('master/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js')}}"></script>
  <script src="{{asset('master/js/owl.carousel.js')}}"></script>
  <!-- jQuery full calendar -->
  <script src="{{asset('master/js/fullcalendar.min.js')}}"></script>
    <!-- Full Google Calendar - Calendar -->
    <script src="{{asset('master/assets/fullcalendar/fullcalendar/fullcalendar.js')}}"></script>
    <!--script for this page only-->
    <script src="{{asset('master/js/calendar-custom.js')}}"></script>
    <script src="{{asset('master/js/jquery.rateit.min.js')}}"></script>
    <!-- custom select -->
    <script src="{{asset('master/js/jquery.customSelect.min.js')}}"></script>
    <script src="{{asset('master/assets/chart-master/Chart.js')}}"></script>
    <!--custome script for all page-->
    <script src="{{asset('master/js/scripts.js')}}"></script>
    <!-- custom script for this page-->
    <script src="{{asset('master/js/sparkline-chart.js')}}"></script>
    <script src="{{asset('master/js/easy-pie-chart.js')}}"></script>
    <script src="{{asset('master/js/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{asset('master/js/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('master/js/xcharts.min.js')}}"></script>
    <script src="{{asset('master/js/jquery.autosize.min.js')}}"></script>
    <script src="{{asset('master/js/jquery.placeholder.min.js')}}"></script>
    <script src="{{asset('master/js/gdp-data.js')}}"></script>
    <script src="{{asset('master/js/morris.min.js')}}"></script>
    <script src="{{asset('master/js/sparklines.js')}}"></script>
    <script src="{{asset('master/js/charts.js')}}"></script>
    <script src="{{asset('master/js/jquery.slimscroll.min.js')}}"></script>
    <script src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
      $(document).ready( function () {
          $('#tableOrders').DataTable();
      } );
      //knob
      $(function() {
        $(".knob").knob({
          'draw': function() {
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
        $("#owl-slider").owlCarousel({
          navigation: true,
          slideSpeed: 300,
          paginationSpeed: 400,
          singleItem: true

        });
      });

      //custom select box

      $(function() {
        $('select.styled').customSelect();
      });

      /* ---------- Map ---------- */
      $(function() {
        $('#map').vectorMap({
          map: 'world_mill_en',
          series: {
            regions: [{
              values: gdpData,
              scale: ['#000', '#000'],
              normalizeFunction: 'polynomial'
            }]
          },
          backgroundColor: '#eef3f7',
          onLabelShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
          }
        });
      });

      {{-- ajax Form Add Post--}}
      $(document).on('click','.create-modal', function() {
        $('#create').modal('show');
        $('.form-horizontal').show();
        $('.modal-title').text('Add Post');
      });
      $("#add").click(function() {
        $.ajax({
          type: 'POST',
          url: 'addMenu',
          data: {
            '_token': $('input[name=_token]').val(),
            'makanan': $('input[name=makanan]').val(),
            'harga': $('input[name=harga]').val(),
            'minuman': $('input[name=minuman]').val(),
            'harga_minum': $('input[name=harga_minum]').val(),
            'status': $('select[name=status]').val()
          },
          success: function(data){
            if ((data.errors)) {
              $('.error').removeClass('hidden');
              $('.error').text(data.errors.makanan);
              $('.error').text(data.errors.harga);
              $('.error').text(data.errors.minuman);
              $('.error').text(data.errors.harga_minum);
            } else {
              $('.error').remove();
              $('#table').append("<tr class='post" + data.id + "'>"+
              "<td>" + data.id + "</td>"+
              "<td>" + data.makanan + "</td>"+
              "<td>" + data.harga + "</td>"+
              "<td>" + data.minuman + "</td>"+
              "<td>" + data.harga_minum + "</td>"+
              "<td>" + data.status + "</td>"+
              "<td> <button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-makanan='" + data.makanan + "' data-harga='" + data.harga + "' data-minuman='" + data.minuman + "' data-harga_minum='" + data.harga_minum + "' data-status='" + data.status + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-makanan='" + data.makanan + "' data-harga='" + data.harga + "' data-minuman='" + data.minuman + "' data-harga_minum='" + data.harga_minum + "' data-status='" + data.status + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-makanan='" + data.makanan + "' data-harga='" + data.harga + "' data-minuman='" + data.minuman + "' data-harga_minum='" + data.harga_minum + "' data-status='" + data.status + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+
              "</tr>");
            }
          },
        });
        $('#makanan').val('');
        $('#harga').val('');
        $('#minuman').val('');
        $('#harga_minum').val('');
        $('#status').val('');
      });

    // function Edit POST
    $(document).on('click', '.edit-modal', function() {
    $('#footer_action_button').text(" Update Post");
    $('#footer_action_button').addClass('glyphicon-check');
    $('#footer_action_button').removeClass('glyphicon-trash');
    $('.actionBtn').addClass('btn-success');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Post Edit');
    $('.deleteContent').hide();
    $('.form-horizontal').show();
    $('#fid').val($(this).data('id'));
    $('#menu_mkn').val($(this).data('makanan'));
    $('#harga_mkn').val($(this).data('harga'));
    $('#menu_mnm').val($(this).data('minuman'));
    $('#harga_mnm').val($(this).data('harga_minum'));
    $('#sts').val($(this).data('status'));
    $('#myModal').modal('show');
    });

    $('.modal-footer').on('click', '.edit', function() {
      $.ajax({
        type: 'POST',
        url: 'editMenu',
        data: {
    '_token': $('input[name=_token]').val(),
    'id': $("#fid").val(),
    'makanan': $('#menu_mkn').val(),
    'harga': $('#harga_mkn').val(),
    'minuman': $('#menu_mnm').val(),
    'harga_minum': $('#harga_mnm').val(),
    'status': $('#sts').val()
    },
    success: function(data) {
          $('.post' + data.id).replaceWith(" "+
          "<tr class='post" + data.id + "'>"+
          "<td>" + data.id + "</td>"+
          "<td>" + data.makanan + "</td>"+
          "<td>" + data.harga + "</td>"+
          "<td>" + data.minuman + "</td>"+
          "<td>" + data.harga_minum + "</td>"+
          "<td>" + data.status + "</td>"+
     "<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-makanan='" + data.makanan + "' data-harga='" + data.harga + "' data-minuman='" + data.minuman + "' data-harga_minum='" + data.harga_minum + "' data-status='" + data.status + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-makanan='" + data.makanan + "' data-harga='" + data.harga + "' data-minuman='" + data.minuman + "' data-harga_minum='" + data.harga_minum + "' data-status='" + data.status + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-makanan='" + data.makanan + "' data-harga='" + data.harga + "' data-minuman='" + data.minuman + "' data-harga_minum='" + data.harga_minum + "' data-status='" + data.status + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+
          "</tr>");
        }
      });
    });


    // form Delete function
    $(document).on('click', '.delete-modal', function() {
    $('#footer_action_button').text(" Delete");
    $('#footer_action_button').removeClass('glyphicon-check');
    $('#footer_action_button').addClass('glyphicon-trash');
    $('.actionBtn').removeClass('btn-success');
    $('.actionBtn').addClass('btn-danger');
    $('.actionBtn').addClass('delete');
    $('.modal-title').text('Delete Post');
    $('.id').text($(this).data('id'));
    $('.deleteContent').show();
    $('.form-horizontal').hide();
    $('.title').html($(this).data('title'));
    $('#myModal').modal('show');
    });

    $('.modal-footer').on('click', '.delete', function(){
      $.ajax({
        type: 'POST',
        url: 'hapusMenu',
        data: {
          '_token': $('input[name=_token]').val(),
          'id': $('.id').text()
        },
        success: function(data){
           $('.post' + $('.id').text()).remove();
        }
      });
    });

      // Show function
      $(document).on('click', '.show-modal', function() {
      $('#show').modal('show');
      $('#i').text($(this).data('id'));
      $('#mkn').text($(this).data('makanan'));
      $('#hmkn').text($(this).data('harga'));
      $('#mnm').text($(this).data('minuman'));
      $('#hmnm').text($(this).data('harga_minum'));
      $('#sts').text($(this).data('status'));
      $('.modal-title').text('Show Post');
      });
    </script>

</body>

</html>