@include('cashier::layouts.header')
@include('cashier::layouts.left')
@yield('content')
@include('cashier::layouts.footer')
