    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        
        <!-- ========================== MANAGEMENT NAVBAR ======================== -->
        <ul class="sidebar-menu">
          <li class="nav-sidebar">
            <a href="{{ route('home.cashier') }}">
                <i class="icon_house_alt"></i>
                <span>Dashboard</span>
            </a>
          </li>
          <li class="nav-sidebar">
            <a href="{{ route('menu') }}">
                <i class="icon_document_alt"></i>
                <span>Menu</span>
            </a>
          </li>
          <li class="nav-sidebar">
            <a href="{{ url('cashier/pesanan') }}">
                <i class="icon_document_alt"></i>
                <span>Buat Pesanan</span>
            </a>
          </li>
          <li class="nav-sidebar">
            <a href="{{ route('transaksi.client') }}">
                <i class="icon_document_alt"></i>
                <span>Transaksi</span>
            </a>
          </li>
        </ul>        
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
