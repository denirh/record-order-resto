@extends('cashier::layouts.app')
@section('content')
	<section id="main-content">
		<section class="wrapper">
			@if(session('msg'))
				<div class="alert alert-success">
					<p>{{ session('msg') }}</p>
				</div>
			@endif
			
			<div class="row">
	          <div class="col-lg-12">
	            <section class="panel">
	              <header class="panel-heading">
	                Daftar History Pesanan Restoran
	              </header>

	              <table class="table table-striped table-advance table-hover">
	                <tbody>
	                  <tr>
	                    <th> No. Pesanan</th>
	                    <th> Total Bayar</th>
	                    <th><i class="icon_cogs"></i> Action</th>
	                  </tr>
	                  @foreach($transaksi as $val)
	                  <tr>
	                    <td>{{ $val->transactions_no_pesanan }}</td>
	                    <td>{{ $val->transactions_harga }}</td>
	                    <td>
	                      <div class="btn-group">
	                      	<a class="show-modal btn btn-info" data-toggle="modal" href="#showTransaksi"><i class="fa fa-eye"></i></a>
	                        <a class="btn btn-danger" href="#"><i class="icon_close_alt2"></i></a>
	                      </div>
	                    </td>
	                  </tr>
	                  @endforeach
	                </tbody>
	              </table>
	            </section>
	          </div>
	        </div>

	        {{-- Modal Form Show POST --}}
			<div id="showTransaksi" class="modal fade" role="dialog">
			  <div class="modal-dialog" style="left: 40px;">
			    <div class="modal-content">
			      	<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal">&times;</button>
			          	<h4 class="modal-title"></h4>
			        </div>
			        @foreach($info_transaksi as $value)
		                <div class="modal-body">
		                    <div class="form-group">
		                      <label for="">Customer :</label>
		                      <span style="font-weight: 700">{{ $value->orders_customer }}</span>
		                    </div>
		                    <div class="form-group">
		                      <label for="">No. Pesanan :</label>
		                      <span style="font-weight: 700">{{ $value->transactions_no_pesanan }}</span>
		                    </div>
		                    <div class="form-group">
		                      <label for="">No. Meja :</label>
		                      <span style="font-weight: 700">{{ $value->orders_no_meja }}</span>
		                    </div>
		                    <div class="form-group">
		                      <label for="">Total Bayar :</label>
		                      <span style="font-weight: 700">{{ $value->transactions_harga }}</span>
		                    </div>
			            </div>
			        @endforeach
			        </div>
			    </div>
			</div>

		</section>
	</section>
@endsection