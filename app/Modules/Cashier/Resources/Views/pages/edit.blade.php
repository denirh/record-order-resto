@extends('cashier::layouts.app')
@section('content')
	<section id="main-content">
		<section class="wrapper">

			<div class="row">
			  <div class="col-md-8 col-md-offset-2">
			    <div class="panel panel-default">
			      <div class="panel-header">
			        <h4 class="panel-title text-center" style="padding: 12px; font-weight: 700">Informasi Pesanan</h4>
			      </div>
			      <div class="panel-body">
	                <div class="form">
	                	{{-- @foreach($orders as $key) --}}
	                  <form class="form-validate form-horizontal" id="feedback_form" method="post" action="{{ url('cashier/update') }}">
	                  	{{ csrf_field() }}
	                    <div class="form-group ">
	                      <label for="customer" class="control-label col-lg-2">Customer</label>
	                      <div class="col-lg-10">
	                        <input class="form-control" value="{{ $id->orders_customer }}"  name="customer" />
	                      </div>
	                    </div>
	                    <div class="form-group ">
	                      <label for="pesan" class="control-label col-lg-2">No. Pesanan</label>
	                      <div class="col-lg-10">
	                        <input class="form-control " value="{{ $id->orders_no_pesanan }}" disabled />
	                        <input type="hidden" value="{{ $id->orders_no_pesanan }}" name="no_pesanan" />
	                      </div>
	                    </div>
	                    <div class="form-group ">
	                      <label for="meja" class="control-label col-lg-2">No. Meja</label>
	                      <div class="col-lg-10">
	                        <input class="form-control " value="{{ $id->orders_no_meja }}" name="no_meja" />
	                      </div>
	                    </div>
						
						<!-- menu start-->
						<div class="form-group ">
				          <div class="col-sm-12">
				            <section class="panel panel-info">
				              <header class="panel-heading">
				                Daftar Menu
				              </header>
				              <table class="table">
				                <thead>
				                  <tr>
				                    <th width="300"> Menu</th>
				                    <th> Harga</th>
				                    <th width="200"> Jumlah (Porsi)</th>
				                  </tr>
				                </thead>
				                <tbody>
				                	@php
				                		$index=array();
				                	@endphp
				                	@foreach($orders as $key)
				                  <tr>
				                  	@php 
				                  		array_push($index,$key->menus_desc);
				                  	@endphp
				                    <td>{{ $key->menus_desc }} </td>
				                    <td>{{ $key->menus_harga }}</td>
				                    <td>				                    	
										<input type="number" value="{{ $key->orders_jumlah_pesan }}" name="{{ $key->orders_menu_id }}" >
				                    </td>
				                  </tr>
				                  	@endforeach
				                  	@foreach($menu as $key)
				                  		@if(in_array($key->menus_desc, $index))
				                  	@else
								  <tr>
				                    <td>{{ $key->menus_desc }} </td>
				                    <td>{{ $key->menus_harga }}</td>
				                    <td>				                    	
										<input type="number" name="{{ $key->menus_id }}" >
				                    </td>
				                  </tr>
				                  @endif
				                  @endforeach

				                </tbody>
				              </table>
				            </section>
				          </div>
				        </div>			                    

						<div class="form-group">
	                      <div class="col-lg-12">
	                        <button class="btn btn-primary btn-block" type="submit">Save</button>
	                      </div>
	                    </div>	                    
	                  </form>
	                  {{-- @endforeach --}}
	                </div>
	            </div>
	           </div>
	    	  </div>
			</div>

		</section>
	</section>
@endsection