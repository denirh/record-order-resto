@extends('cashier::layouts.app')
@section('content')
	<section id="main-content">
		<section class="wrapper">
			
			<div class="row">
	          <div class="col-lg-12">
	            <section class="panel">
	              <header class="panel-heading">
	                Daftar Menu Restoran
	              </header>

	              <table class="display" id="tableOrders">
	              	<thead>
	                
	                  <tr>
	                    <th> Jenis</th>
	                    <th> Desc</th>
	                    <th> Harga</th>
	                    <th> Status</th>
	                  </tr>
	                </thead>
	                
					<tbody>
	                @foreach ($menu as $value)
	                  <tr>
	                    <td>{{ $value->menus_jenis }}</td>
	                    <td>{{ $value->menus_desc }}</td>
	                    <td>{{ $value->menus_harga }}</td>
	                    <td>{{ $value->menus_status }}</td>
	                  </tr>
	                @endforeach 

	                </tbody>
	              </table>
	            </section>

	            {{-- {{$menu->links()}} --}}

	          </div>
	        </div>

		</section>
	</section>
@endsection