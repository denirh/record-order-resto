@extends('cashier::layouts.app')
@section('content')
	<section id="main-content">
		<section class="wrapper">

			@if(session('msg'))
				<div class="alert alert-success">
					<p>{{ session('msg') }}</p>
				</div>
			@endif
			
			<a class="btn btn-success" data-toggle="modal" href="#create-pesanan" style="margin: 20px 0;">
	            Tambah Menu
	        </a>

			<div class="row">
	          <div class="col-lg-12">
	            <section class="panel">
	              <header class="panel-heading">
	                Advanced Table
	              </header>

	              <table class="display " id="tableOrders">
	                <thead>
	                  <tr>
	                    <th> Customer</th>
	                    <th> No. Pesanan</th>
	                    <th> No. Meja</th>
	                    <th><i class="icon_cogs"></i> Action</th>
	                  </tr>
	                </thead>
	                <tbody>
	                  @foreach($orders as $val)
	                  <tr>
	                    <td>{{ $val->orders_customer }}</td>
	                    <td>{{ $val->orders_no_pesanan }}</td>
	                    <td>{{ $val->orders_no_meja }}</td>
	                    <td>
	                      <div class="btn-group">
	                        <a class="btn btn-primary" href="{{ url('cashier/pesanan') }}/{{ $val->orders_id }}" ><i class="icon_plus_alt2"></i></a>
	                        <a class="btn btn-success" href="{{ url('cashier/edit') }}/{{ $val->orders_id }}"><i class="icon_check_alt2"></i></a>
	                        <a class="btn btn-danger" onclick="alert('Yakin hapus ?')" href="{{ url('cashier/hapus') }}/{{ $val->orders_id }}"><i class="icon_close_alt2"></i></a>	                     
	                      </div>
	                    </td>
	                  </tr>
	                  @endforeach
	                </tbody>
	              </table>

	            </section>
	          </div>
	        </div>
        	<!-- page end-->

			{{-- modal create data --}}
			<div id="create-pesanan" class="modal fade" role="dialog">
			  <div class="modal-dialog" style="left: 40px;">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Buat Pesanan Baru</h4>
			      </div>
			      <div class="modal-body">
	                <div class="form">
	                  <form class="form-validate form-horizontal" id="feedback_form" method="post" action="{{ url('cashier/pesanan') }}">
	                  	{{ csrf_field() }}
	                    <div class="form-group ">
	                      <label for="customer" class="control-label col-lg-2">Customer</label>
	                      <div class="col-lg-10">
	                        <input class="form-control" id="customer" name="customer" type="text" required />
	                      </div>
	                    </div>
	                    <div class="form-group ">
	                      <label for="pesan" class="control-label col-lg-2">No. Pesanan</label>
	                      <div class="col-lg-10">
	                        <input class="form-control " id="pesan" type="text" name="no_pesanan" placeholder="ERP<?= date('dmY'); ?> - " required />
	                      </div>
	                    </div>
	                    <div class="form-group ">
	                      <label for="meja" class="control-label col-lg-2">No. Meja</label>
	                      <div class="col-lg-10">
	                        <input class="form-control " id="meja" type="text" name="no_meja" />
	                      </div>
	                    </div>
						
						<!-- menu start-->
						<div class="form-group ">
				          <div class="col-sm-12">
				            <section class="panel panel-info">
				              <header class="panel-heading">
				                Daftar Menu
				              </header>
				              <table class="table">
				                <thead>
				                  <tr>
				                    <th width="300"> Menu</th>
				                    <th> Harga</th>
				                    <th width="200"> Jumlah (Porsi)</th>
				                  </tr>
				                </thead>
				                <tbody>
				                	@foreach($menu as $value)
				                  <tr>
				                    <td>{{ $value->menus_desc }} <input type="hidden" name="{{ $value->menus_id }}" value="{{ $value->menus_id }}"></td>
				                    <td>{{ $value->menus_harga }}</td>
				                    <td>
										<input type="number" name="{{ $value->menus_id }}" id="j_porsi">
				                    </td>
				                  </tr>
				                  	@endforeach
				                </tbody>
				              </table>
				            </section>
				          </div>
				        </div>			                    

	                    <div class="form-group">
	                      <div class="col-lg-12">
	                        <button class="btn btn-primary btn-block" type="submit">Save</button>
	                      </div>
	                    </div>
	                  </form>
	                </div>
	            </div>
	           </div>
	    	  </div>
			</div>
			              

		</section>
	</section>
@endsection