<?php

namespace App\Modules\Cashier\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $fillable = ['transactions_id', 'transactions_harga', 'transactions_status', 'transactions_no_pesanan' ];
    protected $primaryKey = 'transactions_id';

    // public function users()
    // {
    //     return $this->belongsToMany('App\Modules\Auth\Models\User', 'transactions_kasir_id', 'users_id');
    // }

    // public function orders()
    // {
    // 	return $this->belongsToMany('App\Modules\Cashier\Models\Order', 'transactions_orders_id', 'orders_id');
    // }
}
