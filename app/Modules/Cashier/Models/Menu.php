<?php

namespace App\Modules\Cashier\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    protected $table = 'menus';
    protected $fillable = [ 'menus_id', 'menus_jenis', 'menus_desc', 'menus_harga', 'menus_status' ];
    protected $primaryKey = 'menus_id';

    public function orders()
    {
    	return $this->hasMany('App\Modules\Cashier\Models\Order', 'orders_menu_id', 'menus_id');
    }
}
