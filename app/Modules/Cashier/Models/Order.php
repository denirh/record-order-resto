<?php

namespace App\Modules\Cashier\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
	protected $dates = ['deleted_at'];
	
    protected $table = 'orders';
    protected $fillable = [ 'orders_id', 'orders_customer', 'orders_no_pesanan', 'orders_no_meja', 'orders_menu_id', 'orders_jumlah_pesan', ];
    protected $primaryKey = 'orders_id';


    public function menus()
    {
    	return $this->belongsToMany('App\Modules\Cashier\Models\Menu', 'orders_menu_id', 'menus_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Modules\Cashier\Models\Transaction', 'transactions_orders_id', 'transactions_id');
    }
}
