    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        
        <!-- ========================== MANAGEMENT NAVBAR ======================== -->
        <ul class="sidebar-menu">
          <li class="nav-sidebar">
            <a class="" href="{{ route('home') }}">
                <i class="icon_house_alt"></i>
                <span>Dashboard</span>
            </a>
          </li>
          <li class="nav-sidebar">
            <a class="" href="{{ route('pesanan.client') }}">
                <i class="icon_document_alt"></i>
                <span>Pesanan</span>
            </a>
          </li>
          <li class="nav-sidebar">
            <a class="" href="{{ route('menu.resto') }}">
                <i class="icon_document_alt"></i>
                <span>Menu</span>
            </a>
          </li>
        </ul>        
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
