@include('waiter::layouts.header')
@include('waiter::layouts.left')
@yield('content')
@include('waiter::layouts.footer')
