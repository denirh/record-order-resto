@extends('waiter::layouts.app')
@section('content')
	<section id="main-content">
		<section class="wrapper">
			@if(session('msg'))
				<div class="alert alert-success">
					<p>{{ session('msg') }}</p>
				</div>
			@endif

			<a class="btn btn-success" data-toggle="modal" href="#myModal" style="margin: 20px 0;">
	            Tambah Menu
	        </a>
			
			<div class="row">
	          <div class="col-lg-12">
	            <section class="panel">
	              <header class="panel-heading">
	                Daftar Menu Restoran
	              </header>

	              <table class="table table-striped table-advance table-hover">
	                <tbody>
	                  <tr>
	                    <th> Makanan</th>
	                    <th> Harga</th>
	                    <th> Minuman</th>
	                    <th> Harga</th>
	                    <th> Status</th>
	                    <th><i class="icon_cogs"></i> Action</th>
	                  </tr>
	                  <tr>
	                    <td>Angeline Mcclain</td>
	                    <td>2004-07-06</td>
	                    <td>dale@chief.info</td>
	                    <td>Rosser</td>
	                    <td>176-026-5992</td>
	                    <td>
	                      <div class="btn-group">
	                        <a class="btn btn-success" href="#"><i class="fa fa-edit"></i></a>
	                        <a class="btn btn-danger" href="#"><i class="icon_close_alt2"></i></a>
	                      </div>
	                    </td>
	                  </tr>
	                </tbody>
	              </table>
	            </section>
	          </div>
	        </div>


    		<!-- Modal Add Data -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" style="margin-left: 400px; left: 0;">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Masukkan Menu</h4>
                  </div>
                  <div class="modal-body">
                    <form action="" method="post">
                    	{{ csrf_field() }}
                        <div class="form-group create-data">
                          <label>Makanan</label>
                          <input class="form-control data-input" name="makanan" type="text" placeholder="Masukkan Makanan" required="" value="{{ old('makanan') }}">
                        </div>
                        <div class="form-group create-data">
                          <label>Harga</label>
                          <input class="form-control data-input" name="harga" type="number" placeholder="Masukkan Harga" required="" value="{{ old('harga') }}">
                        </div>                                             
                        <div class="form-group create-data">
                          <label>Minuman</label>
                          <input class="form-control data-input" name="minuman" type="text" placeholder="Masukkan Minuman" required="" value="{{ old('minuman') }}">
                        </div>
                        <div class="form-group create-data">
                          <label>Harga</label>
                          <input class="form-control data-input" name="harga_minum" type="number" placeholder="Masukkan Harga" required="" value="{{ old('harga_minum') }}"> 
                        </div>
                        <div class="form-group create-data">
                          <label>Status</label>
                          <select class="form-control" name="status">
                              <option value="proses">Proses</option>
                              <option value="ready">Ready</option>
                          </select>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                    <button class="btn btn-success" type="submit">Save changes</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
            <!-- end modal -->


		</section>
	</section>
@endsection