@extends('waiter::layouts.app')
@section('content')
	<section id="main-content">
		<section class="wrapper">
			@if(session('msg'))
				<div class="alert alert-success">
					<p>{{ session('msg') }}</p>
				</div>
			@endif
			
			<h1>Waiter Page</h1>

		</section>
	</section>
@endsection