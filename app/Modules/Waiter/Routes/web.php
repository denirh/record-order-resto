<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'waiter'], function () {
    Route::get('/', 'WaiterController@index')->name('home');
    Route::get('/pesanan', 'WaiterController@pesanan')->name('pesanan.client');
    Route::get('/menu', 'WaiterController@menu')->name('menu.resto');
    Route::get('/logout', 'WaiterController@logout');
    Route::get('/login', 'WaiterController@login')->name('login');
});
