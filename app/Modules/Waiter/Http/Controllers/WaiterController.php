<?php

namespace App\Modules\Waiter\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WaiterController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth', ['except' => ['login']]);
        $this->middleware('role:pelayan', ['except' => ['login']]);
	}

    public function index()
    {
    	return view('waiter::pages.home');
    }

    public function login()
    {
    	return redirect('/auth/login');
    }

    public function pesanan()
    {
        return view('waiter::pages.pesanan');
    }

    public function menu()
    {
        return view('waiter::pages.menu');
    }

    public function logout()
    {
    	Auth::logout();
    	return redirect('/auth/login');
    }
}
