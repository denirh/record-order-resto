<?php

namespace App\Modules\Auth\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    protected $primaryKey = 'id';

    protected $fillable = [
        'roles_id', 'username', 'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Modules\Auth\Models\Role', 'roles_id', 'id');
    }

    public static function punyaRole($roleName)
    {
        // if($this->role->roleName == $roleName){
        //     return true;
        // }
        //     return false;
        $id = \Auth::user()->id;
        return User::find($id)->role()->first()->roleName == $roleName ? true : false;
    }

    public function transactions()
    {
        return $this->hasMany('App\Modules\Cashier\Models\Transaction', 'transactions_kasir_id', 'users_id');
    }
}
