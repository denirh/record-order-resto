<?php

namespace App\Modules\Auth\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $primaryKey = 'id';
    protected $fillable = ['roleName'];

    public function user()
    {
    	return $this->hasMany('App\Modules\Auth\Models\User', 'roles_id', 'id');
    }
}
