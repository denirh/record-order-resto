<?php

namespace App\Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Auth\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function registerForm()
    {
        return view('auth::auth.register');
    }

    public function store(Request $request)
    {
        $roles = DB::table('roles')->select('id')->where('roleName','=', 'admin')->first()->id; 

        $data = User::create([
            'roles_id'  => $roles,
            'username'  => $request->username,
            'name'      => $request->nama,
            'email'     => $request->email,
            'password'  => bcrypt($request->password)
        ]);

        $data->save();

        return redirect('/auth/login')->with('msg', 'Berhasil buat akun !');
    }
}
