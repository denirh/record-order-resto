<?php

namespace App\Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    public function __construct()
    {
        // $this->middleware('guest');
    }

    public function loginForm()
    {
        return view('auth::auth.login');
    }

    public function loginUser(Request $request)
    {
    	if(Auth::attempt([
    		'email' => $request->userEmail,
    		'password' => $request->password
    	])){
    		// return redirect('/waiter');
    	}elseif(Auth::attempt([
    		'username' => $request->userEmail,
    		'password' => $request->password
    	])){
    		// return redirect('/waiter');
    	}else{
    		return 'salah bro';
    	}

        $role = Auth::user()->role()->first()->roleName;

        switch ($role) {
            case 'kasir':
                return redirect('/cashier');
                break;
            
            default:
                return redirect('/admin');
                break;
        }
    }

    public function danger()
    {
        return view('auth::pages.403');
    }
}
