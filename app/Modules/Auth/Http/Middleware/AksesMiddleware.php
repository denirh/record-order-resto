<?php

namespace App\Modules\Auth\Http\Middleware;

use App\Modules\Auth\Models\User;
use Closure;

class AksesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roleName)
    {
        // if(auth()->check() && !auth()->user()->punyaRole($roleName)){
        //     return view('auth::pages.403');                                
        // }
        if(\Auth::check() && !User::punyaRole($roleName)){
            return response(view('auth::pages.403'));                                
        }
        return $next($request);
    }
}
