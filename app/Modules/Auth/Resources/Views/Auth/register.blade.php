@extends('auth::layouts.app')
@section('content')
	<div class="container">

	    <form class="login-form" action="{{ route('register.user') }}" method="POST" style="margin-top: 100px;">
	    	{{ csrf_field() }}
	      <div class="login-wrap">
	        <p class="login-img"><i class="icon_lock_alt"></i></p>
	        <div class="input-group">
	          <span class="input-group-addon"><i class="icon_profile"></i></span>
	          <input type="text" class="form-control" placeholder="Nama" name="nama" autofocus required="">
	        </div>
	        <div class="input-group">
	          <span class="input-group-addon"><i class="icon_profile"></i></span>
	          <input type="text" class="form-control" placeholder="Username" name="username" autofocus required="">
	        </div>
	        <div class="input-group">
	          <span class="input-group-addon"><i class="icon_mail"></i></span>
	          <input type="email" class="form-control" placeholder="Email" name="email" autofocus required="">
	        </div>
	        <div class="input-group">
	          <span class="input-group-addon"><i class="icon_key_alt"></i></span>
	          <input type="password" class="form-control" placeholder="Password" name="password" required="">
	        </div>
	        <div class="input-group">
	          <span class="input-group-addon"><i class="icon_key_alt"></i></span>
	          <input type="password" class="form-control" placeholder="Konfirmasi Password" name="repassword">
	        </div>
	        <button class="btn btn-info btn-lg btn-block" type="submit">Signup</button>
	      </div>
	    </form>
	</div>
@endsection