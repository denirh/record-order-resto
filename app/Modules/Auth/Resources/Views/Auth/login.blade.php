@extends('auth::layouts.app')
@section('content')

	<div class="container">
		@if(session('msg'))
				<div class="alert alert-success">
					<p>{{ session('msg') }}</p>
				</div>
			@endif
	    <form class="login-form" action="{{ route('login.user') }}" method="POST" style="margin-top: 160px;">
		{{ csrf_field() }}
	      <div class="login-wrap">
	        <p class="login-img"><i class="icon_lock_alt"></i></p>
	        <div class="input-group">
	          <span class="input-group-addon"><i class="icon_profile"></i></span>
	          <input type="text" name="userEmail" class="form-control" placeholder="Username or Email" autofocus>
	        </div>
	        <div class="input-group">
	          <span class="input-group-addon"><i class="icon_key_alt"></i></span>
	          <input type="password" name="password" class="form-control" placeholder="Password">
	        </div>
	        <label class="checkbox">
	                <input type="checkbox" value="remember-me"> Remember me
	                <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
	            </label>
	        <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
	        <button class="btn btn-info btn-lg btn-block" type="submit">Signup</button>
	      </div>
	    </form>
	</div>

@endsection