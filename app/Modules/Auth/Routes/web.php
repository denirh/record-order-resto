<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::get('/', function () {
        dd('This is the auth module index page. Build something great!');
    });
    Route::get('/login', 'LoginController@loginForm');
    Route::post('/login', 'LoginController@loginUser')->name('login.user');
    Route::get('/register', 'RegisterController@registerForm');
    Route::post('/register', 'RegisterController@store')->name('register.user');
    Route::get('/403', 'LoginController@danger');
});
